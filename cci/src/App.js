import React,{Component} from 'react';
import {BrowserRouter as Router,Route} from 'react-router-dom';
import First from './components/First';
import Header from './components/layout/Header';
import About from './components/pages/About';
import Login from './components/pages/Login';

import './App.css';

class App extends Component{
  render(){
    return(
      <Router>
        <div className="App">
        <Header />
        <Route path="/first" component={First} />
        <Route path="/about" component={About} />
        <Route path="/login" component={Login} />
      </div>

      </Router>

    )
  }
}
export default App;
