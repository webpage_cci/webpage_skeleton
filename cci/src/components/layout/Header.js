import React from 'react';
import{ Link } from 'react-router-dom';

function Header(){
    return(
    <header style={headerStyle}>
    
    
    <Link style={linkStyle} to="/first">Home</Link> 
    <Link style={linkStyle} to="/about">About</Link>
    <Link style={linkStyle} to="/login">Login</Link> 
    
    </header>
    )
    }
    
    const headerStyle = {
    textAlign: 'right',
    background: '#333',
    color: '#fff',
    padding: '10px'
    }
    
    const linkStyle = {
    padding: '20px',
    color: '#fff',
    textDecoration: 'none'
    }
    

export default Header;