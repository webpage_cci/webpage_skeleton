import React from 'react';
function About(){
    return(
        <div>
        <h1>About US</h1>
        <br></br>
           <p>Let’s say one of your friends posted a photograph on Facebook. Now you go and like the image and then you started checking out the comments too. Now while you are browsing over comments you see that likes count has increased by 100, since you liked the picture, even without reloading the page. This magical count change is because of Reactjs.</p> 
        <p>React is a declarative, efficient, and flexible JavaScript library for building user interfaces. It’s ‘V’ in MVC. ReactJS is an open-source, component-based front end library responsible only for the view layer of the application. It is maintained by Facebook.</p>

        <p>React uses a declarative paradigm that makes it easier to reason about your application and aims to be both efficient and flexible. It designs simple views for each state in your application, and React will efficiently update and render just the right component when your data changes. The declarative view makes your code more predictable and easier to debug.</p>
        </div>
    )
}


export default About;